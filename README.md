# Marmaduke
REST APIs - Documentation 

**REAL TIME DATA**

|API | URL | Description|
|:----|:-----|:------------|
|Moored now|/api/moored_now/:id_portinformer/|Get portinformer's currently moored|
|At roadstead now|/api/roadstead_now/:id_portinformer/|Get portinformer's currently at roadstead|
|Arrivals|/api/arrivals_now/:id|Get portinformer's arrivals today| 
|Departures|/api/departures_now/:id    | Get portinformer's departures today| 
|Arrival previsions today|/api/arrival_previsions_now/:id    |Get portinformer's arrival previsions  |
|Traffic list|/api/traffic_list_now/:id    |Get portinformer's traffic list data   |
|Commercial operations|/api/shipped_goods_now/:id    |Get portinformer's shipped goods data    |


**ARCHIVE DATA**

|API | URL | Description|
|:----|:-----|:------------|
|Moored|/api/moored/:start/:stop/:id_portinformer/|Get portinformer moored|
|At roadstead|/api/roadstead/:start/:stop/:id_portinformer/|Get portinformer at roadstead|
|Arrival previsions|    |    |
|Arrivals|    |    | 
|Departures|    |    | 
|Traffic list|    |    |
|Commercial operations|    |    |
